"""
Ejercicio 1
Parte 2
"""
print("Programa que identifica el tipo de dato de un valor ingresado por el usuario, se realizarán cinco interaciciones:")

valor = input("Primera Interacción, ingrese un valor cualquiera: ")
print("Este tipo de dato en Python es:")
print(type(valor))

valor = input("Segunda Interacción, ingrese un valor cualquiera: ")
print("Este tipo de dato en Python es:")
print(type(valor))

valor = input("Tercera Interacción, ingrese un valor cualquiera: ")
print("Este tipo de dato en Python es:")
print(type(valor))

valor = input("Cuarta Interacción, ingrese un valor cualquiera: ")
print("Este tipo de dato en Python es:")
print(type(valor))

valor = input("Quinta Interacción, ingrese un valor cualquiera: ")
print("Este tipo de dato en Python es:")
print(type(valor))

print("¡YA NO SE HARÁN MÁS INTERACCIONES!")
