"""
Ejercicio 1
Parte 1
"""
# Mostrar mensaje introductorio
print("Empezando a trabajar con Python")
print("Realizado por: Harold")
print("Consultando los tipos de valores:")

# Mostrar los distintos tipos de datos con la instruccion type
# 1. Inicialiazar dato
# 2. Mostrar en pantalla información al usuario
# 3. Mostrar en pantalla el tipo de dato correspondiente

numero_int = 875
print("El tipo de dato de " + str(numero_int) + " es: ")
print(type(numero_int))

numero_float = 4.89
print("El tipo de dato de " + str(numero_float) + " es: ")
print(type(numero_float))

str_ = "Now is better than never."
print("El tipo de dato del texto: " + str_ + " es: ")
print(type(str_))

numero_float = 1.32
print("El tipo de dato de " + str(numero_float) + " es: ")
print(type(numero_float))

# Mostara la respuesta mediante el método isinstance()
complejo = 5+8j
print("¿El valor " + str(complejo) + " corresponde a un valor entero?" )
print(isinstance(complejo, int))

numero = 8.2
print("¿El valor " + str(numero) + " corresponde a un valor entero?" )
print(isinstance(numero, int))

texto = "Readability counts."
print("¿El texto: " + texto + " corresponde a un string?")
print(isinstance(texto, str))
